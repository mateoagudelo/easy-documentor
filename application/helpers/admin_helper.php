<?php

if (!function_exists('get_font_family')) {
	function get_font_family() {
		echo '<link href="https://fonts.googleapis.com/css?family=Quicksand" rel="stylesheet">';
	}
}

if (!function_exists('get_public_font_family')) {
	function get_public_font_family() {
		echo '<link href="https://fonts.googleapis.com/css?family=Bitter:700" rel="stylesheet">';
	}
}

if (!function_exists('get_public_font_family_2')) {
	function get_public_font_family_2() {
		echo '<link href="https://fonts.googleapis.com/css?family=Noto+Serif" rel="stylesheet">';
	}
}

if (!function_exists('get_public_font_family_3')) {
	function get_public_font_family_3() {
		echo '<link href="https://fonts.googleapis.com/css?family=Abel" rel="stylesheet">';
	}
}

if (!function_exists('admin_login_set_email')) {
	function admin_login_set_email() {
		return array('type' => 'input', 'id' => 'email', 'name' => 'email', 'class' => 'form-control', 'required' => 'on', 'autofocus' => 'on', 'value' => set_value('email'));
	}
}

if (!function_exists('admin_login_set_password')) {
	function admin_login_set_password() {
		return array('type' => 'password', 'id' => 'password', 'name' => 'password', 'class' => 'form-control', 'required' => 'on', 'autofocus' => 'on', 'value' => set_value('password'));
	}
}

if (!function_exists('admin_login_set_input')) {
	function admin_login_set_input() {
		return array('type' => 'submit', 'name' => 'login', 'value' => 'Login', 'class' => 'btn btn-success btn-sm');
	}
}

if (!function_exists('link_tag_js')) {
	function link_tag_js($name) {
		echo '<script type="text/javascript" src="'.base_url().$name.'" ></script>';
	}
}

if (!function_exists('link_tag_script')) {
	function link_tag_script($name) {
		echo '<script src="'.$name.'"></script>';
	}
}


if (!function_exists('menu')) {
	function menu($num) {

$option = '';
$option1 = '';

if ($num == 1) {
	$option = '<li class="active"><a href="'.base_url('administrator/projects/').'"><b class="glyphicon glyphicon-folder-open"></b> Projects</a></li><li><a href="'.base_url('administrator/messages/').'"><b class="glyphicon glyphicon-envelope"></b> Messages</a></li><li><a href="'.base_url('administrator/media/').'"><b class="glyphicon glyphicon-picture"></b> Media</a></li>';
	$config = '<li><a href="'.base_url('administrator/config/').'"><b class="glyphicon glyphicon-cog"></b> Site settings</a></li>';
} elseif ($num == 2) {
	$option = '<li><a href="'.base_url('administrator/projects/').'"><b class="glyphicon glyphicon-folder-open"></b> Projects</a></li><li class="active"><a href="'.base_url('administrator/messages/').'"><b class="glyphicon glyphicon-envelope"></b> Messages</a></li><li><a href="'.base_url('administrator/media/').'"><b class="glyphicon glyphicon-picture"></b> Media</a></li>';
	$config = '<li><a href="'.base_url('administrator/config/').'"><b class="glyphicon glyphicon-cog"></b> Site settings</a></li>';
} elseif ($num == 3) {
	$option = '<li><a href="'.base_url('administrator/projects/').'"><b class="glyphicon glyphicon-folder-open"></b> Projects</a></li><li><a href="'.base_url('administrator/messages/').'"><b class="glyphicon glyphicon-envelope"></b> Messages</a></li><li><a href="'.base_url('administrator/media/').'"><b class="glyphicon glyphicon-picture"></b> Media</a></li>';
	$config = '<li><a href="'.base_url('administrator/config/').'"><b class="glyphicon glyphicon-cog"></b> Site settings</a></li>';
} elseif ($num == 4) {
	$option = '<li><a href="'.base_url('administrator/projects/').'"><b class="glyphicon glyphicon-folder-open"></b> Projects</a></li><li><a href="'.base_url('administrator/messages/').'"><b class="glyphicon glyphicon-envelope"></b> Messages</a></li><li class="active"><a href="'.base_url('administrator/media/').'"><b class="glyphicon glyphicon-picture"></b> Media</a></li>';
	$config = '<li><a href="'.base_url('administrator/config/').'"><b class="glyphicon glyphicon-cog"></b> Site settings</a></li>';
} elseif($num == 5) {
	$option = '<li><a href="'.base_url('administrator/projects/').'"><b class="glyphicon glyphicon-folder-open"></b> Projects</a></li><li><a href="'.base_url('administrator/messages/').'"><b class="glyphicon glyphicon-envelope"></b> Messages</a></li><li><a href="'.base_url('administrator/media/').'"><b class="glyphicon glyphicon-picture"></b> Media</a></li>';

	$config = '<li class="active"><a href="'.base_url('administrator/config/').'"><b class="glyphicon glyphicon-cog"></b> Site settings</a></li>';
}

$menu = '<nav class="navbar navbar-default" role="navigation">
<div class="navbar-header">
<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
<span class="sr-only">Menú</span>
<span class="icon-bar"></span>
<span class="icon-bar"></span>
<span class="icon-bar"></span>
</button>
<a class="navbar-brand" href="'.base_url('administrator/projects/').'">Easy Documentor</a>
</div>
 
<div class="collapse navbar-collapse navbar-ex1-collapse">
<ul class="nav navbar-nav">
'.$option.'
</ul>
 
<ul class="nav navbar-nav navbar-right">
'.$config.'
<li class="dropdown">
<a href="#" class="dropdown-toggle" data-toggle="dropdown"><b class="glyphicon glyphicon-user"></b> My account</a>
<ul class="dropdown-menu">
<li><a href="'.base_url('administrator/my_account/').'"><b class="glyphicon glyphicon-pencil"></b> Profile</a></li>
<li class="divider"></li>
<li><a href="'.base_url('administrator/logout/').'"><b class="glyphicon glyphicon-off"></b> Logout</a></li>
</ul>
</li>
</ul>
</div>
</nav>';


echo $menu;

	}
}

/* --------------------------------- admin new --------------------------------------------- */ 

if (!function_exists('admin_New_Project_Name')) {
	function admin_New_Project_Name() {
		return array('type' => 'input', 'id' => 'name', 'name' => 'name', 'class' => 'form-control input-lg', 'required' => 'on', 'autofocus' => 'on', 'value' => set_value('name'));
	}
}

if (!function_exists('admin_New_Project_Slug')) {
	function admin_New_Project_Slug() {
		return array('type' => 'input', 'id' => 'slug', 'name' => 'slug', 'class' => 'form-control input-sm', 'required' => 'on', 'value' => set_value('slug'));
	}
}

if (!function_exists('admin_New_Project_Content')) {
	function admin_New_Project_Content() {
		return array('rows' => 2, 'id' => 'content', 'name' => 'content', 'class' => 'form-control', 'required' => 'on', 'value' => set_value('content'));
	}
}

if (!function_exists('admin_New_Project_Submit')) {
	function admin_New_Project_Submit() {
		return array('type' => 'submit', 'class' => 'btn btn-success btn-sm', 'value' => 'Save');
	}
}

/* --------------------------------- admin edit --------------------------------------------- */ 

if (!function_exists('admin_Edit_Project_Name')) {
	function admin_Edit_Project_Name($name) {
		return array('type' => 'input', 'id' => 'name', 'name' => 'name', 'class' => 'form-control input-lg', 'required' => 'on', 'autofocus' => 'on', 'value' => $name);
	}
}

if (!function_exists('admin_Edit_Project_Slug')) {
	function admin_Edit_Project_Slug($slug) {
		return array('type' => 'input', 'id' => 'slug', 'name' => 'slug', 'class' => 'form-control input-sm', 'required' => 'on', 'value' => $slug, 'readonly' => 'on');
	}
}

if (!function_exists('admin_Edit_Project_Content')) {
	function admin_Edit_Project_Content($content) {
		return array('rows' => 2, 'id' => 'content', 'name' => 'content', 'class' => 'form-control', 'required' => 'on', 'value' => $content);
	}
}

if (!function_exists('admin_Edit_Project_Hidden')) {
	function admin_Edit_Project_Hidden($var) {
		return array('type' => 'hidden', 'id' => 'rev', 'name' => 'rev', 'value' => $var);
	}
}

if (!function_exists('admin_Edit_Project_Visibility')) {
	function admin_Edit_Project_Visibility($value) {

		if ($value == 1) {
			echo '<select name="visibility" class="form-control"><option value="1" selected>Public</option><option value="0">Private</option></select>';
		} elseif ($value == 0) {
		echo '<select name="visibility" class="form-control"><option value="1">Public</option><option value="0" selected>Private</	option></select>';
		} else {
		echo '<div class="alert alert-danger"><strong>Oops! </strong>We have problems</div>';
		}

	}
}

if (!function_exists('admin_Edit_Project_Form')) {
	function admin_Edit_Project_Form($value) {

		if ($value == 1) {
		echo '<select name="form" class="form-control"><option value="1" selected>Active</option><option value="0">Disable</option></select>';
		} elseif ($value == 0) {
		echo '<select name="form" class="form-control"><option value="1">Active</option><option value="0" selected>Disable</option></select>';
		} else {
		echo '<div class="alert alert-danger"><strong>Oops! </strong>We have problems</div>';
		}

	}
}

if (!function_exists('admin_Edit_Project_Submit')) {
	function admin_Edit_Project_Submit() {
		return array('type' => 'submit', 'class' => 'btn btn-warning btn-sm', 'value' => 'Update');
	}
}

/* --------------------------------- admin new section --------------------------------------------- */ 

if (!function_exists('admin_New_Section_Name')) {
	function admin_New_Section_Name() {
		return array('type' => 'input', 'id' => 'name_section', 'name' => 'name_section', 'class' => 'form-control', 'required' => 'on', 'autofocus' => 'on', 'value' => set_value('name_section'), 'minlength' => '5');
	}
}

if (!function_exists('admin_New_Section_Slug')) {
	function admin_New_Section_Slug() {
		return array('type' => 'input', 'id' => 'slug_section', 'name' => 'slug_section', 'class' => 'form-control', 'required' => 'on', 'value' => set_value('slug_section'), 'minlength' => '5');
	}
}

if (!function_exists('admin_New_Section_Submit')) {
	function admin_New_Section_Submit() {
		return array('type' => 'submit', 'class' => 'btn btn-success btn-xs', 'value' => 'Save');
	}
}

/* --------------------------------- admin edit section --------------------------------------------- */ 

if (!function_exists('admin_edit_section_name')) {
	function admin_edit_section_name($value) {
		return array('type' => 'input', 'id' => 'name_section', 'name' => 'name_section', 'class' => 'form-control', 'required' => 'on', 'autofocus' => 'on', 'value' => $value);
	}
}

if (!function_exists('admin_edit_section_slug')) {
	function admin_edit_section_slug($value) {
		return array('type' => 'input', 'id' => 'slug_section', 'name' => 'slug_section', 'class' => 'form-control input-sm', 'required' => 'on', 'readonly' => 'on', 'value' => $value);
	}
}

if (!function_exists('admin_edit_section_content')) {
	function admin_edit_section_content($content) {
		return array('rows' => 2, 'id' => 'content_section', 'name' => 'content_section', 'class' => 'form-control', 'required' => 'on', 'value' => $content);
	}
}

if (!function_exists('admin_edit_section_visibility')) {
	function admin_edit_section_visibility($value) {
		if ($value == 1) {
			echo '<select name="visibility" class="form-control"><option value="1" selected>Public</option><option value="0">Private</option></select>';
		} elseif ($value == 0) {
			echo '<select name="visibility" class="form-control"><option value="1">Public</option><option value="0" selected>Private</option></select>';
		} else {
			echo '<div class="alert alert-danger"><strong>Oops! </strong>We have problems</div>';
		}
	}
}

if (!function_exists('admin_edit_section_submit')) {
	function admin_edit_section_submit() {
		return array('type' => 'submit', 'class' => 'btn btn-warning btn-xs', 'value' => 'Save');
	}
}

/* --------------------------------- admin new section --------------------------------------------- */ 

if (!function_exists('admin_delete_section_submit')) {
	function admin_delete_section_submit() {
		return array('type' => 'submit', 'class' => 'btn btn-danger btn-sm', 'value' => 'Confirm');
	}
}

/* --------------------------------- admin profile --------------------------------------------- */ 

if (!function_exists('admin_profile_name')) {
	function admin_profile_name($value) {
		return array('type' => 'input', 'id' => 'name', 'name' => 'name', 'class' => 'form-control', 'required' => 'on', 'value' => $value);
	}
}

if (!function_exists('admin_profile_lastname')) {
	function admin_profile_lastname($value) {
		return array('type' => 'input', 'id' => 'lastname', 'name' => 'lastname', 'class' => 'form-control', 'required' => 'on', 'value' => $value);
	}
}

if (!function_exists('admin_profile_email')) {
	function admin_profile_email($value) {
		return array('type' => 'input', 'class' => 'form-control', 'readonly' => 'on', 'value' => $value);
	}
}

if (!function_exists('admin_profile_submit')) {
	function admin_profile_submit() {
		return array('type' => 'submit', 'class' => 'btn btn-warning btn-sm', 'value' => 'Save');
	}
}

/* --------------------------------- admin profile > password --------------------------------------------- */ 

if (!function_exists('admin_profile_old_password')) {
	function admin_profile_old_password() {
		return array('type' => 'password', 'id' => 'old_password', 'name' => 'old_password', 'class' => 'form-control', 'required' => 'on', 'value' => set_value('old_password'));
	}
}

if (!function_exists('admin_profile_new_password')) {
	function admin_profile_new_password() {
		return array('type' => 'password', 'id' => 'new_password', 'name' => 'new_password', 'class' => 'form-control', 'required' => 'on', 'value' => set_value('new_password'));
	}
}

if (!function_exists('admin_profile_new_password_repeat')) {
	function admin_profile_new_password_repeat() {
		return array('type' => 'password', 'id' => 'new_password_repeat', 'name' => 'new_password_repeat', 'class' => 'form-control', 'required' => 'on', 'value' => set_value('new_password_repeat'));
	}
}

if (!function_exists('admin_profile__password_submit')) {
	function admin_profile__password_submit() {
		return array('type' => 'submit', 'class' => 'btn btn-danger btn-sm', 'value' => 'Update');
	}
}

/* --------------------------------- user register --------------------------------------------- */

if (!function_exists('admin_register_name')) {
	function admin_register_name() {
		return array('type' => 'input', 'id' => 'name', 'name' => 'name', 'class' => 'form-control', 'required' => 'on', 'value' => set_value('name'), 'autofocus' => 'on');
	}
}

if (!function_exists('admin_register_lastname')) {
	function admin_register_lastname() {
		return array('type' => 'input', 'id' => 'lastname', 'name' => 'lastname', 'class' => 'form-control', 'required' => 'on', 'value' => set_value('lastname'));
	}
}

if (!function_exists('admin_register_email')) {
	function admin_register_email() {
		return array('type' => 'input', 'id' => 'email', 'name' => 'email', 'class' => 'form-control', 'required' => 'on', 'value' => set_value('email'));
	}
}

if (!function_exists('admin_register_password')) {
	function admin_register_password() {
		return array('type' => 'password', 'id' => 'password', 'name' => 'password', 'class' => 'form-control', 'required' => 'on', 'value' => set_value('password'));
	}
}

if (!function_exists('admin_register_password_repeat')) {
	function admin_register_password_repeat() {
		return array('type' => 'password', 'id' => 'password_repeat', 'name' => 'password_repeat', 'class' => 'form-control', 'required' => 'on', 'value' => set_value('password_repeat'));
	}
}

if (!function_exists('admin_register_submit')) {
	function admin_register_submit() {
		return array('type' => 'submit', 'class' => 'btn btn-success btn-sm', 'value' => 'Register!');
	}
}

/* --------------------------------- site config --------------------------------------------- */

if (!function_exists('admin_config_title')) {
	function admin_config_title($value) {
		return array('type' => 'text', 'id' => 'title', 'name' => 'title', 'class' => 'form-control', 'value' => $value, 'required' => 'on');
	}
}

if (!function_exists('admin_config_meta_description')) {
	function admin_config_meta_description($value) {
		return array('row' => '1', 'id' => 'description', 'name' => 'description', 'class' => 'form-control', 'value' => $value, 'required' => 'on');
	}
}

if (!function_exists('admin_config_meta_autor')) {
	function admin_config_meta_autor($value) {
		return array('type' => 'text', 'id' => 'autor', 'name' => 'autor', 'class' => 'form-control', 'value' => $value, 'required' => 'on');
	}
}

if (!function_exists('admin_config_redirect')) {
	function admin_config_redirect($value) {
		return array('type' => 'text', 'id' => 'redirect', 'name' => 'redirect', 'class' => 'form-control', 'value' => $value, 'required' => 'on');
	}
}

if (!function_exists('admin_config_submit')) {
	function admin_config_submit() {
		return array('type' => 'submit', 'class' => 'btn btn-success btn-xs', 'value' => 'Save');
	}
}