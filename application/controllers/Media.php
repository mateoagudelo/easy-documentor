<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Media extends CI_Controller
{

	public function __construct()
    {
		parent::__construct();
		$this->load->library(array('form_validation', 'pagination'));
		$this->load->helper(array('form','url','html','admin','security'));
	}

	private function secure()
    {

		if ($this->session->userdata('logged')) {
			return true;
		} else {
			return false;
		}

	}

    private function id()
    {
        return $this->session->userdata('id');
    }

	public function index()
    {

		if ($this->secure()) {

			$this->load->model('Upload_Model');
			$config['base_url'] = base_url().'administrator/media/';
			$config['total_rows'] = $this->Upload_Model->f_Images($this->id());
			$this->pagination->initialize($config);		
			$data['images'] = $this->Upload_Model->total_Pagination_Images($this->id(), $this->pagination->per_page, $this->uri->segment(3));

			$this->load->view('admin_media/admin_media_index', $data);
		} else {
			redirect(base_url('administrator/'));
		}

	}

    public function do_upload()
    {
	    $this->form_validation->set_rules('title', 'Title', 'required|min_length[5]|max_length[255]|trim|xss_clean');

        if ($this->form_validation->run() == TRUE) {

            $config['upload_path'] = './uploads/';
            $config['allowed_types'] = 'gif|jpg|png';
            $config['max_size'] = 2000;
            $config['max_width'] = 2024;
            $config['max_height'] = 2008;
            $this->load->library('upload', $config);

        	if (!$this->upload->do_upload('userfile')) {
            	$error = array('error' => $this->upload->display_errors());
            	$this->load->view('admin_media/admin_media_index', $error);
        	} else {
            	$file_info = $this->upload->data();
            	$data = array('upload_data' => $this->upload->data());
            	$title = $this->input->post('title');
            	$image = $file_info['file_name'];
            	$this->load->model('Upload_Model');
            	$up = $this->Upload_Model->set_Info_Query($this->id(), $title, $image);
            	redirect(base_url('administrator/media/'));
        	}
        } else {
            $this->index();
        }

    }

    protected $id;
    public function view_Image($id)
    {
        $this->id = $id;
        if ($this->secure()) {
            $this->load->model('Upload_Model');
            $data['images'] = $this->Upload_Model->get_Image($this->id, $this->id());

            if (!empty($data['images'])) {
                $this->load->view('admin_media/admin_media_view_image', $data);
            } else {
                redirect(base_url('administrator/media/'));
            }

        } else {
            redirect(base_url('administrator/'));
        }

    }

    public function delete_Image($id)
    {

        if ($this->secure()) {
            $this->id = $id;
            $this->load->model('Upload_Model');
            $uri = $this->Upload_Model->delete_Image($this->id, $this->id());

            foreach ($uri as $key) {
                unlink('./uploads/'.$key->uri);
            }

            $this->Upload_Model->delete_Image_2($this->id, $this->id());
            redirect(base_url('administrator/media/'));
        } else {
            redirect(base_url('administrator/'));
        }
        
    }

}