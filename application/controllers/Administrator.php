<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Administrator extends CI_Controller
{

	public function __construct()
    {
		parent::__construct();
		$this->load->library(array('form_validation', 'pagination'));
		$this->load->helper(array('form','url','html','admin','security'));
	}

	protected $status, $data_user, $data;

	private function secure()
    {
		if ($this->session->userdata('logged')) {
			return true;
		} else {
			return false;
		}

	}

	private function id()
    {
		return $this->session->userdata('logged');
	}

	public function site_Config()
    {

		if ($this->secure()) {

			$this->load->model('Administrator_Model');
			$this->data['configs'] = $this->Administrator_Model->config_Site();
			
			if ($this->input->post()) {
				$this->form_validation->set_rules('title', 'Title', 'required|max_length[100]xss_clean');
				$this->form_validation->set_rules('description', 'Meta description', 'required|max_length[100]|xss_clean');
				$this->form_validation->set_rules('autor', 'Meta autor', 'required|max_length[50]|xss_clean');

				if ($this->form_validation->run()) {
					$name = $this->input->post('title', true);
					$desc = $this->input->post('description', true);
					$autor = $this->input->post('autor', true);
					$redi = $this->input->post('redirect', true);
					$this->Administrator_Model->update_Config_Site($name, $desc, $autor, $redi);
					redirect(base_url('administrator/config/'));
				} else {
					$this->load->view('admin/admin_config_site', $this->data);
				}
			
			} else {
				$this->load->view('admin/admin_config_site', $this->data);
			}
			
		} else {
			redirect(base_url('administrator/projects/'));
		}
		
	}

	public function index()
    {

	if (!$this->secure()) {	

		if ($this->input->post()) {
			$this->form_validation->set_rules('email', 'E-mail', 'trim|required|valid_email|min_length[5]|max_length[150]|xss_clean');
			$this->form_validation->set_rules('password', 'Password', 'required|min_length[5]|xss_clean');

			if ($this->form_validation->run()) {
				$this->load->model('Administrator_Model');
				$this->status = $this->Administrator_Model->login($this->input->post('email'), $this->input->post('password'));

				if ($this->status) {

					foreach ($this->status as $this->data) {
						$this->data_user = array (
        					'id'  		=> $this->data->id,
        					'email'    	=> $this->data->email,
       						'logged' 	=> TRUE
						);
					}

					$this->session->set_userdata($this->data_user);
					redirect(base_url().'administrator/projects/');
				} else {
					$this->load->view('admin/admin_login');
				}
			} else {
				$this->load->view('admin/admin_login');
			}

		} else {
			$this->load->view('admin/admin_login');
		}

	} else {
		redirect(base_url('administrator/projects/'));
		}

	}

	public function logout()
    {
		$this->session->sess_destroy();
		redirect(base_url('administrator/'));
	}


	public function register_Install()
    {

		if (!$this->secure()) {
			if ($this->input->post()) {
				$this->form_validation->set_rules('name', 'Name', 'required|min_length[5]|max_length[100]|xss_clean');
				$this->form_validation->set_rules('lastname', 'Lastname', 'required|min_length[5]|max_length[100]|xss_clean');
				$this->form_validation->set_rules('email', 'Content', 'trim|valid_email|required|min_length[5]|max_length[100]|xss_clean');
				$this->form_validation->set_rules('password', 'Password', 'required|min_length[5]|max_length[100]|xss_clean');
				$this->form_validation->set_rules('password_repeat', 'Password repeat', 'required|min_length[5]|max_length[100]|matches[password]|xss_clean');

				if ($this->form_validation->run()) {
					$this->load->model('Administrator_Model');
					$this->status = $this->Administrator_Model->register_Install($this->input->post());
					if ($this->status) {
						redirect(base_url('administrator/'));
					} else {
						$this->data['title'] = 'Account already exist!';
						$this->data['problem'] = 'It was detected that an administrator already exists.';
						$this->load->view('admin/admin_errors', $this->data);
					}
					
				} else {
					$this->load->view('admin/admin_register');
				}
				
			} else {
				$this->load->view('admin/admin_register');
			}
		}

	}

	public function projects()
    {

		if ($this->secure()) {
			$this->load->model('Administrator_Model');
			$config['base_url'] = base_url().'administrator/projects/';
			$config['total_rows'] = $this->Administrator_Model->f_Projects($this->id());
			$this->pagination->initialize($config);		
			$data['projects'] = $this->Administrator_Model->total_Pagination_Projects($this->session->userdata('id'), $this->pagination->per_page, $this->uri->segment(3));
			$data['model'] = $this->Administrator_Model;

			$this->load->view('admin/admin_panel', $data);
		} else {
			redirect(base_url().'administrator/');
		}

	}

	protected $name, $slug, $content, $form, $visibility;
	public function new_Project()
    {

		if ($this->secure()) {

			if ($this->input->post()) {
				$this->form_validation->set_rules('name', 'Name', 'required|min_length[5]|max_length[150]|xss_clean');
				$this->form_validation->set_rules('slug', 'Slug', 'trim|required|min_length[5]|max_length[100]|xss_clean');
				$this->form_validation->set_rules('content', 'Content', 'required|min_length[5]|max_length[50000]|xss_clean');

				if ($this->form_validation->run()) {
					$this->load->model('Administrator_Model');

					$this->name = $this->input->post('name', true);
					$this->slug = $this->input->post('slug', true);
					$this->content = $this->input->post('content', true);
					$this->status = $this->Administrator_Model->new_Project($this->id(), $this->name, $this->slug, $this->content);

					if ($this->status) {
						redirect(base_url('administrator/projects/'));
					} else {
						$this->data['msj'] = 'Slug already exist!';
						$this->data['color'] = 'danger';
						$this->load->view('admin/admin_new_project', $this->data);
					}
					
				} else {
					$this->load->view('admin/admin_new_project');
				}

			} else {
				$this->load->view('admin/admin_new_project');
			}		

		} else {
			redirect(base_url('administrator/'));
		}

	}


	public function edit_Project($id)
    {

		if ($this->secure()) {
			$this->load->model('Administrator_Model');
			$this->data['projects'] = $this->Administrator_Model->get_Project($id, $this->id());
			$this->data['sections'] = $this->Administrator_Model->get_Sections($this->id(), $id);

			if ($this->input->post()) {
				if ($this->input->post('rev') == 1) {

					$this->form_validation->set_rules('name', 'Name', 'required|min_length[5]|max_length[150]|xss_clean');
					$this->form_validation->set_rules('slug', 'Slug', 'trim|required|min_length[5]|max_length[100]|xss_clean');
					$this->form_validation->set_rules('content', 'Content', 'required|min_length[5]|max_length[50000]|xss_clean');
					$this->form_validation->set_rules('visibility', 'Visibility', 'required|numeric|xss_clean');
					$this->form_validation->set_rules('form', 'Contact form', 'required|numeric|xss_clean');
					if ($this->form_validation->run()) {
						$this->name = $this->input->post('name', true);
						$this->content = $this->input->post('content', true);
						$this->form = $this->input->post('form', true);
						$this->Administrator_Model->edit_Project($id, $this->name, $this->content, $this->input->post('visibility'), $this->form);
						redirect(base_url('administrator/projects/edit/'.$id.'/'));
					} else {
						$this->load->view('admin/admin_edit_project', $this->data);
					}

				} elseif ($this->input->post('rev') == 2) {
					
					$this->form_validation->set_rules('name_section', 'Name', 'required|min_length[5]|max_length[150]|xss_clean');
					$this->form_validation->set_rules('slug_section', 'Slug', 'required|min_length[5]|max_length[100]|xss_clean');
					if ($this->form_validation->run()) {
						$name_section = $this->input->post('name_section', true);
						$slug_section = $this->input->post('slug_section', true);
						$this->Administrator_Model->new_Section($this->id(), $id, $name_section, $slug_section);
						redirect(base_url('administrator/projects/edit/'.$id.'/'));
					} else {
						redirect(base_url('administrator/projects/edit/'.$id.'/'));
					}	
				}
				
			} else {
				if (!empty($this->data['projects'])) {
					$this->load->view('admin/admin_edit_project', $this->data);
				} else {
					redirect(base_url('administrator/projects/'));
				}
			}

		} else {
			redirect(base_url('administrator/'));
		}
		
	}

	public function delete_Project($id)
    {

		if ($this->secure()) {
			$this->id = $id;
			$this->data['id'] = $this->id;

			if ($this->input->post()) {
				$this->load->model('Administrator_Model');
				$this->Administrator_Model->delete_Project($this->id(), $this->id);
				redirect(base_url('administrator/projects/'));
			} else {
				$this->load->view('admin/admin_delete_project', $this->data);
			}
			
		} else {
			redirect(base_url('administrator/'));
		}
		
	}


	/* sections */

	protected $id;
	public function section_Edit($id)
    {
		$this->id = $id;

		if ($this->secure()) {
			$this->load->model('Administrator_Model');
			$this->data['sections'] = $this->Administrator_Model->get_Section($this->id(), $this->id);

			if ($this->input->post()) {
				$this->form_validation->set_rules('name_section', 'Name', 'required|min_length[5]|max_length[150]|xss_clean');
				$this->form_validation->set_rules('content_section', 'Content', 'required|min_length[5]|max_length[50000]|xss_clean');
				$this->form_validation->set_rules('visibility', 'Visibility', 'required|numeric|xss_clean');
				
				if ($this->form_validation->run()) {
					$this->name = $this->input->post('name_section', true);
					$this->content = $this->input->post('content_section', true);
					$this->visibility = $this->input->post('visibility', true);
					$this->Administrator_Model->update_Section($this->id, $this->id(), $this->name, $this->content, $this->visibility);
					redirect(base_url('administrator/sections/edit/'.$this->id.'/'));
				} else {
					$this->load->view('admin/admin_edit_section', $this->data);
				}

			} else {
				$this->load->view('admin/admin_edit_section', $this->data);
			}
			
		} else {
			redirect(base_url('administrator/'));
		}
		
	}


	public function section_Delete($id)
    {
		$this->id = $id;

		if ($this->secure()) {
			$this->data['id'] = $this->id;

			if ($this->input->post()) {
				$this->form_validation->set_rules('id_section', 'Section', 'required|numeric|xss_clean');

				if ($this->form_validation->run()) {
					$this->load->model('Administrator_Model');
					$this->Administrator_Model->delete_Section($this->id, $this->id());
					redirect('administrator/projects/');
				} else {
					redirect('administrator/projects/');
				}
			} else {
				$this->load->view('admin/admin_delete_section', $this->data);
			}
			
		} else {
			redirect(base_url('administrator/'));
		}

	}


	public function messages()
    {

		if ($this->secure()) {

			$this->load->model('Administrator_Model');
			$config['base_url'] = base_url().'administrator/messages/';
			$config['total_rows'] = $this->Administrator_Model->f_Messages($this->id());
			$this->pagination->initialize($config);		
			$data['messages'] = $this->Administrator_Model->total_Pagination_Messages($this->id(), $this->pagination->per_page, $this->uri->segment(3));
			$data['model'] = $this->Administrator_Model;

			$this->load->view('admin/admin_messages', $data);
		} else {
			redirect(base_url('administrator/'));
		}

	}

	public function view_Message($id)
    {

		if ($this->secure()) {
			$this->id = $id;
			$this->load->model('Administrator_Model');
			$this->data['messages'] = $this->Administrator_Model->get_Message($this->id(), $this->id);
			$this->data['model'] = $this->Administrator_Model;

			if (!empty($this->data['messages'])) {
				$this->load->view('admin/admin_view_message', $this->data);
			} else {
				redirect(base_url('administrator/messages/'));
			}

		} else {
			redirect(base_url('administrator/'));
		}

	}

	public function delete_Message($id)
    {

		if ($this->secure()) {
			$this->id = $id;
			$this->load->model('Administrator_Model');
			$this->Administrator_Model->delete_Message($this->id(), $this->id);
			redirect(base_url('administrator/messages/'));
		} else {
			redirect(base_url('administrator/'));
		}

	}

	public function delete_All_Messages()
    {

		if ($this->secure()) {
			$this->load->model('Administrator_Model');
			$this->Administrator_Model->delete_All($this->id());
			redirect(base_url('administrator/messages/'));
		} else {
			redirect(base_url('administrator/'));
		}

	}

	/* end sections */


	/* user account */

	public function get_Profile()
    {

		if ($this->secure()) {
			$this->load->model('Administrator_Model');
			$this->data['users'] = $this->Administrator_Model->get_Profile($this->id());

			if ($this->input->post()) {

				$this->form_validation->set_rules('name', 'Name', 'required|min_length[4]|max_length[100]|xss_clean');
				$this->form_validation->set_rules('lastname', 'Lastname', 'required|min_length[4]|max_length[100]|xss_clean');

				if ($this->form_validation->run()) {
					$name = $this->input->post('name', true);
					$lastname = $this->input->post('lastname', true);
					$this->Administrator_Model->update_Profile($this->id(), $name, $lastname);
					redirect(base_url('administrator/my_account/'));
				} else {
					$this->load->view('admin/admin_profile_user', $this->data);
				}

			} else {
				$this->load->view('admin/admin_profile_user', $this->data);
			}
			

		} else {
			redirect(base_url('administrator/'));
		}
		
	}

	public function get_Password()
    {

		if ($this->secure()) {

			if ($this->input->post()) {

				$this->form_validation->set_rules('old_password', 'Old password', 'required|xss_clean');
				$this->form_validation->set_rules('new_password', 'New password', 'required|min_length[5]|xss_clean');
				$this->form_validation->set_rules('new_password_repeat', 'New password repeat', 'required|min_length[5]|matches[new_password]|xss_clean');

				if ($this->form_validation->run()) {
					$this->load->model('Administrator_Model');
					$old_password = $this->input->post('old_password', true);
					$new_password = $this->input->post('new_password', true);
					$result = $this->Administrator_Model->update_Password($this->id(), $old_password, $new_password);

					if ($result) {
						redirect(base_url('administrator/logout/'));
					} else {
						$data['message_alert'] = 'Error when changing the password';
						$this->load->view('admin/admin_profile_user_password', $data);
					}
					
				} else {
					$this->load->view('admin/admin_profile_user_password');
				}

			} else {
				$this->load->view('admin/admin_profile_user_password');
			}
			
		} else {
			redirect(base_url('administrator/'));
		}

	}

	public function install()
    {
		$this->load->library('migration');

		if(!$this->migration->version(7)){
			show_error($this->migration->error_string());
		}else{
			redirect(base_url('administrator/register'));
		}

	}

}
