<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller
{
	public function __construct()
    {
		parent::__construct();
		$this->load->helper(array('url'));
	}

	protected $data;
	public function index()
    {
		$this->load->model('Project_Model');
		$this->data['site_data'] = $this->Project_Model->Get_Site();
		$this->load->view('public/index', $this->data);
	}
}
