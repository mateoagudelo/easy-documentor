<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Project extends CI_Controller
{

	public function __construct()
    {
		parent::__construct();
		$this->load->library(array('form_validation'));
		$this->load->helper(array('form','url','html','admin','security'));
	}

	/* data and resources */
	protected $data, $query, $status;

	/* protocol and urls */
	protected $slug, $section;

	public function index($slug)
    {
		$this->slug = $slug;

		if (!empty($this->slug)) {
			$this->load->model('Project_Model');
			$this->data['site_data'] = $this->Project_Model->get_Site();
			$this->data['content'] = $this->Project_Model->get_Project($this->slug);

			foreach ( $this->data['content'] as $id_p) {
			    $id_project = $id_p->id;
			}

			$this->data['sections'] = $this->Project_Model->get_All_Sections($id_project);

			if (!empty($this->data['content'])) {
				$this->load->view('public/project', $this->data);
			} else {
				redirect(base_url());
			}

		} else {
			redirect(base_url());
		}

	}

	public function section($slug, $section)
    {
		$this->slug = $slug;
		$this->section = $section;

		if (!empty($this->slug) AND !empty($this->section)) {

			$this->load->model('Project_Model');
			$this->data['site_data'] = $this->Project_Model->get_Site();
			$this->data['project_slug'] = $this->slug;
			$this->data['content'] = $this->Project_Model->get_Project_Section($this->slug, $this->section);

			foreach ($this->data['content'] as $id_p) {
			    $id_pr = $id_p->id_project;
			}

			$this->data['sections'] = $this->Project_Model->get_All_Sections($id_pr);
			$this->data['forms'] = $this->Project_Model->get_Form($id_pr);
			$this->query = $this->Project_Model->get_Name_Project($id_pr);

			foreach ($this->query as $dt) {
				$this->data['title_project'] = $dt->name;
			}

			if (!empty($this->data['content'])) {
				$this->load->view('public/section', $this->data);
			} else {
				redirect(base_url());
			}

		} else {
			redirect(base_url());
		}

	}

	public function contact($slug)
    {
		$this->slug = $slug;

		if (!empty($this->slug)) {

			$this->load->model('Project_Model');
			$this->data['content'] = $this->Project_Model->get_Project($this->slug);

			foreach ($this->data['content'] as $id_p) {
			    $id_project = $id_p->id;
			}

			$this->data['sections'] = $this->Project_Model->get_All_Sections($id_project);

			if ($this->input->post()) {
				$this->form_validation->set_rules('name', 'Name', 'required|xss_clean');
				$this->form_validation->set_rules('email', 'Email', 'valid_email|required|xss_clean');
				$this->form_validation->set_rules('message', 'Message', 'required|xss_clean');

				if ($this->form_validation->run()) {
					$this->load->model('Project_Model');
					$this->status = $this->Project_Model->new_Message($this->input->post());

					if ($this->status) {
						redirect(base_url('contact/'.$this->slug.'/'));
					} else {
						$this->load->view('public/contact', $this->data);
					}
					
				} else {
					$this->load->view('public/contact', $this->data);
				}
			} else {

				if (!empty($this->data['content'])) {
					$this->load->view('public/contact', $this->data);
				} else {
					redirect(base_url());
				}

			}	

		} else {
			redirect(base_url());
		}
		
	}

}