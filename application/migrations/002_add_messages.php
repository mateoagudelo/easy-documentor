<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_Messages extends CI_Migration {
	protected $table_messages = 'easy_messages';
	public function up() {

		$this->dbforge->add_field(
			array(
				'id' => array(
					'type' => 'INT',
					'constraint' => 11,
					'unsigned' => TRUE,
					'auto_increment' => TRUE,
				),
				'id_project' => array(
					'type' => 'INT',
					'constraint' => 11,
				),
				'autor' => array(
					'type' => 'INT',
					'constraint' => 11,
				),
				'name' => array(
					'type' => 'VARCHAR',
					'constraint' => 50,
				),
				'email' => array(
					'type' => 'VARCHAR',
					'constraint' => 150,
				),
				'message' => array(
					'type' => 'VARCHAR',
					'constraint' => 200,
				),
				'date' => array(
					'type' => 'VARCHAR',
					'constraint' => 15, 
				),

			)
		);

		$this->dbforge->add_key('id', TRUE); //id como primary_key
		$this->dbforge->create_table($this->table_messages); //creamos la tabla
	}

	public function down() {
		$this->dbforge->drop_table($this->table_messages);
	}

}