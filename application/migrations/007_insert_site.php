<?php defined("BASEPATH") OR exit("No puedes acceder aquí directamente");
 
class Migration_Insert_Site extends CI_Migration {
 
 	protected $table_site = 'easy_site';
 	protected $data;
	public function up() {
		
		$this->data = array(
			'name'					=>		'Easy Documentor',
			'meta_description'		=>		'Meta description (...here...)',
			'meta_autor'			=> 		'Mateo Agudelo',
			'redirect'				=>		'https://codecanyon.net/user/mateoagudelo'
		);
		
		$this->db->insert($this->table_site, $this->data);
	}
 
	public function down() {
		$this->db->where('id', 1);
		$this->db->delete($this->table_site);
	}
 
}