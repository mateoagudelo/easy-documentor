<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_Site extends CI_Migration {
	
	protected $table_site = 'easy_site';
	public function up() {
		$this->dbforge->add_field(
			array(
				'id' 	=>				array(
					'type' 				=>		'INT',
					'constraint'		=>		11,
					'unsigned'			=>		TRUE,
					'auto_increment'	=>		TRUE,					
				),
				'name' 		=>			array(
					'type'				=> 		'VARCHAR',
					'constraint' 		=> 		100,
				),
				'meta_description' 		=> 		array(
					'type'				=>		'VARCHAR',
					'constraint'		=> 		100,
				),
				'meta_autor' 	=>		array(
					'type' 				=>		'VARCHAR',
					'constraint' 		=> 		100,
				),
				'redirect' 	=>			array(
					'type' 				=>		'VARCHAR',
					'constraint' 		=> 		150,
				),
			)
		);

		$this->dbforge->add_key('id', true);
		$this->dbforge->create_table($this->table_site);
	}

	public function down() {
		$this->dbforge->drop_table($this->table_site);
	}
}	