<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_Media extends CI_Migration {
	protected $table_media = 'easy_media';
	public function up() {

		$this->dbforge->add_field(
			array(
				'id'		=>		array(
					'type'				=>		'INT',
					'constraint'		=>		11,
					'unsigned'			=>		TRUE,
					'auto_increment'	=>		TRUE,
 
				),
				"autor"	=>		array(
					"type"				=>		"INT",
					"constraint"		=>		11,
					"auto_increment" 	=>		FALSE
				),
				"title"	=>		array(
					"type"				=>		"VARCHAR",
					"constraint"		=>		255,
				),
				"uri" => 		array(
					"type" 				=> 		"VARCHAR",
					"constraint" 		=> 		300,
				),
			)
		);
 
		$this->dbforge->add_key('id', TRUE); //id como primary_key
		$this->dbforge->create_table($this->table_media); //creamos la tabla
	}

	public function down() {
		$this->dbforge->drop_table($this->table_media);
	}

}