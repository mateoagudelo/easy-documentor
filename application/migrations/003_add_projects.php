<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_Projects extends CI_Migration {
	protected $table_projects = 'easy_projects';

	public function up() {

		$this->dbforge->add_field(
			array(
				'id'		=>		array(
					'type'				=>		'INT',
					'constraint'		=>		11,
					'unsigned'			=>		TRUE,
					'auto_increment'	=>		TRUE,
				),
				'autor' 	=>		array(
					'type' 				=>		'INT',
					'constraint'		=> 		11,
				),		
				'name' 		=>		array(
					'type'				=> 		'VARCHAR',
					'constraint' 		=> 		150,
				),
				'slug' 		=> 		array(
					'type'				=>		'VARCHAR',
					'constraint'		=> 		100,
				),
				'content' 	=>		array(
					'type' 				=>		'VARCHAR',
					'constraint' 		=> 		50000,
				),
				'visibility' =>		array(
					'type' 				=>		'INT',
					'constraint' 		=> 		11,
				),
				'form' =>		array(
					'type' 				=>		'INT',
					'constraint' 		=> 		11,
				),
			)
		);

		$this->dbforge->add_key('id', TRUE);
		$this->dbforge->create_table($this->table_projects);
	}

	public function down() {
		$this->dbforge->drop_table($this->table_projects);
	}
}