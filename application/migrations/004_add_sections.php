<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_Sections extends CI_Migration {

	protected $table_sections = 'easy_sections';
	public function up() {
		$this->dbforge->add_field(
			array(
				'id' 	=>			array(
					'type' 				=>		'INT',
					'constraint'		=>		11,
					'unsigned'			=>		TRUE,
					'auto_increment'	=>		TRUE,					
				),
				'autor' 	=>		array(
					'type' 				=>		'INT',
					'constraint'		=> 		11,
				),
				'id_project' 	=>	array(
					'type' 				=>		'INT',
					'constraint'		=> 		11,
				),	
				'name' 		=>		array(
					'type'				=> 		'VARCHAR',
					'constraint' 		=> 		150,
				),
				'slug' 		=> 		array(
					'type'				=>		'VARCHAR',
					'constraint'		=> 		100,
				),
				'content' 	=>		array(
					'type' 				=>		'VARCHAR',
					'constraint' 		=> 		50000,
				),
				'visibility' 	=>	array(
					'type' 				=>		'INT',
					'constraint'		=> 		11,
				),
			)
		);

		$this->dbforge->add_key('id', true);
		$this->dbforge->create_table($this->table_sections);
	}

	public function down() {
		$this->dbforge->drop_table($this->table_sections);
	}

}
