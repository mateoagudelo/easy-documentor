<?php

class Migration_Add_Users extends CI_Migration {
	protected $table_users = 'easy_users';

	public function up() {
		$this->dbforge->add_field(
			array(
				'id'		=>			array(
					'type'				=>		'INT',
					'constraint'		=>		11,
					'unsigned'			=>		TRUE,
					'auto_increment'	=>		TRUE,
				),
				'email' 		=>		array(
					'type'				=> 		'VARCHAR',
					'constraint' 		=> 		100,
				),
				'password' 		=>		array(
					'type'				=> 		'VARCHAR',
					'constraint' 		=> 		300,
				),
				'name' 		=>		array(
					'type'				=> 		'VARCHAR',
					'constraint' 		=> 		100,
				),
				'lastname' 		=>		array(
					'type'				=> 		'VARCHAR',
					'constraint' 		=> 		150,
				),
			)
		);

		$this->dbforge->add_key('id', true);
		$this->dbforge->create_table($this->table_users);
	}

	public function down() {
		$this->dbforge->drop_table($this->table_users);
	}

}



