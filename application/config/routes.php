<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/

$route['default_controller'] = 'home/index';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

$route['administrator/config'] = 'administrator/site_config';

$route['administrator/projects/(:num)'] = 'administrator/projects';
$route['administrator/projects/new'] = 'administrator/new_project';

$route['administrator/projects/edit/(:num)'] = 'administrator/edit_project/$1';
$route['administrator/projects/delete/(:num)'] = 'administrator/delete_project/$1';

$route['administrator/sections/edit/(:num)'] = 'administrator/section_edit/$1';
$route['administrator/sections/delete/(:num)'] = 'administrator/section_delete/$1';

$route['administrator/messages/(:num)'] = 'administrator/messages';
$route['administrator/messages/delete-all'] = 'administrator/delete_all_messages';
$route['administrator/messages/view/(:num)'] = 'administrator/view_message/$1';
$route['administrator/messages/delete/(:num)'] = 'administrator/delete_message/$1';

$route['administrator/my_account'] = 'administrator/get_profile';
$route['administrator/my_account/password'] = 'administrator/get_password';

$route['administrator/media'] = 'media/index';
$route['administrator/media/(:num)'] = 'media/index';
$route['administrator/media/view/(:num)'] = 'media/view_image/$1';
$route['administrator/media/delete/(:num)'] = 'media/delete_image/$1';

$route['project/(:any)'] = 'project/index/$1';
$route['project/(:any)/(:any)'] = 'project/section/$1/$2';
$route['contact/(:any)'] = 'project/contact/$1';

$route['administrator/install'] = 'administrator/install';
$route['administrator/register'] = 'administrator/register_install';