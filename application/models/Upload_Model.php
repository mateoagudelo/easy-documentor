<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Upload_model extends CI_Model
{
	public function construct()
    {
        parent::__construct();
        $this->load->database();
    }
    
    protected $table_images = 'easy_media';

    public function set_Info_Query($id, $title, $image)
    {

        $data = array(
            'autor' => $id,
            'title' => $title,
            'uri' => $image
        );

        return $this->db->insert($this->table_images, $data);
    }

    protected $id_autor, $id, $query, $row, $data;
    public function f_Images($id)
    {
        $this->id = $id;
        $this->db->where('autor', $this->id);
        $this->query = $this->db->get($this->table_images);
        return  $this->query->num_rows();   
    }

    public function total_Pagination_Images($id, $per_page, $segment)
    {
        $this->id = $id;
        $this->db->select('id, title, uri');
        $this->db->where('autor', $this->id);
        $this->query = $this->db->get($this->table_images, $per_page, $segment);

        if ($this->query->num_rows() > 0) {
            foreach ($this->query->result() as $this->row) {
                $this->data[] = $this->row;
            }
            return $this->data;
        }

    }

    public function get_Image($id, $id_autor)
    {
        $this->id = $id;
        $this->id_autor = $id_autor;

        $this->db->where('id', $this->id);
        $this->db->where('autor', $this->id_autor);
        $this->query = $this->db->get($this->table_images);

        if (!empty($this->query)) {
            return $this->query->result();
        } else {
            return null;
        }

    }

    public function delete_Image($id, $id_autor)
    {
        $this->id = $id;
        $this->id_autor = $id_autor;

        $this->db->select('uri');
        $this->db->where('id', $this->id);
        $this->db->where('autor', $this->id_autor);
        $this->query = $this->db->get($this->table_images);
        return $this->query->result();
    }

    public function delete_Image_2($id, $id_autor)
    {
        $this->db->where('id', $this->id);
        $this->db->where('autor', $this->id_autor);
        $this->db->delete($this->table_images);
        return true;
    }

}