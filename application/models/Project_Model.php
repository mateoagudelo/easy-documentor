<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Project_Model extends CI_Model
{

	public function __construct()
    {
		parent::__construct();
		$this->load->database();
	}

	/* table */
	protected $table_projects = 'easy_projects';
	protected $table_sections = 'easy_sections';
	protected $table_messages = 'easy_messages';
	protected $table_site = 'easy_site';

	/* id's */
	protected $id, $id_project, $id_section;

	/* data */
	protected $data, $data2;

	/* querys and results */
	protected $query, $query_2;

	/* slug secure */
	protected $slug, $section_slug;
	public function get_Project($slug)
    {
		$this->slug = $slug;
		$this->db->where('slug', $this->slug);
		$this->query = $this->db->get($this->table_projects);

		return $this->query->result();
	}

	public function get_Project_Section($slug, $section)
    {
		$this->slug = $slug;
		$this->section_slug = $section;

		$this->db->select('id');
		$this->db->where('slug', $this->slug);
		$this->query = $this->db->get($this->table_projects);

		foreach ($this->query->result() as $this->data) {
			$this->id_project = $this->data->id;
		}

		$this->db->where('id_project', $this->id_project);
		$this->db->where('slug', $this->section_slug);
		$this->query_2 = $this->db->get($this->table_sections);

		foreach ($this->query_2->result() as $this->data2) {
			$this->id_section = $this->data2->id_project;
		}

		if ($this->id_project == $this->id_section) {
			return $this->query_2->result();
		} else {
			return false;
		}

	}

	public function get_All_Sections($id_project)
    {
		$this->id_project = $id_project;

		$this->db->select('name, slug');
		$this->db->where('visibility', 1);
		$this->db->where('id_project', $this->id_project);
		$this->query = $this->db->get($this->table_sections);
		return $this->query->result();
	}

	public function get_Name_Project($id_project)
    {
		$this->id_project = $id_project;

		$this->db->select('name');
		$this->db->where('id', $this->id_project);
		$this->query = $this->db->get($this->table_projects);
		return $this->query->result();
	}


	/* Frontend Message */


	public function new_Message($d = array())
    {
		$this->query = $this->db->select('autor');
		$this->query = $this->query = $this->db->where('id', $d['id_project']);
		$this->query = $this->db->get($this->table_projects);

		foreach ($this->query->result() as $this->data_2) {
			$this->id_user = $this->data_2->autor;
		}

		$d['id_project'] = filter_var($d['id_project'], FILTER_SANITIZE_NUMBER_INT);
		$d['name'] = filter_var($d['name'], FILTER_SANITIZE_STRING);
		$d['name'] = addslashes($d['name']);
		$d['name'] = htmlspecialchars($d['name']);
		$d['email'] = filter_var($d['email'], FILTER_SANITIZE_EMAIL);
		$d['email'] = addslashes($d['email']);
		$d['message'] = filter_var($d['message'], FILTER_SANITIZE_STRING);
		$d['message'] = addslashes($d['message']);
		$d['message'] = htmlspecialchars($d['message']);

		$this->data = array(
			'id_project' => $d['id_project'],
			'autor' => $this->id_user,
			'name' => $d['name'],
			'email' => $d['email'],
			'message' => $d['message'],
			'date' => date("j/n/Y")
		);

		$this->db->insert($this->table_messages, $this->data);
		return true;
	}

	public function get_Form($id_project)
    {
		$this->id_project = $id_project;

		$this->db->select('form');
		$this->db->where('id', $this->id_project);
		$this->query = $this->db->get($this->table_projects);
		return $this->query->result();
	}

	public function get_Site()
    {
		$this->db->where('id', 1);
		$this->query = $this->db->get($this->table_site);
		return $this->query->result();
	}

}