<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Administrator_Model extends CI_Model
{

	public function __construct()
    {
		$this->load->database();
	}

	/* vars database */
	protected $table_users = 'easy_users';
	protected $table_projects = 'easy_projects';
	protected $table_sections = 'easy_sections';
	protected $table_messages = 'easy_messages';
	protected $table_site = 'easy_site';

	/* vars more important */
	protected $email, $password;

	/* results and querys */
	protected $query, $data, $row;

	/* users */
	protected $id, $id_user, $id_autor;

	public function login($email, $password)
    {
		$this->email = $email;
		$this->password = sha1($password);

		$this->db->where('email', $this->email);
		$this->db->where('password', $this->password);
		$this->query = $this->db->get($this->table_users);

		if ($this->query->num_rows()==1) {
			return $this->query->result();
			return true;
		} else { 
			return false;
		}

	}

	public function register_Install($data = array())
    {
		$this->query = $this->db->get($this->table_users);

		if ($this->query->num_rows()==0) {
			$this->data = $data;
			unset($this->data['password_repeat']);
			$this->data['password'] = sha1($this->data['password']);
			print_r($this->data);
			$this->db->insert($this->table_users, $this->data);
			return true;
		} else {
		    return false;
		}

	}

	public function config_Site()
    {
		$this->db->where('id', 1);
		$this->query = $this->db->get($this->table_site);
		return $this->query->result();
	}

	public function update_Config_Site($name, $description, $autor, $redirect)
    {
		$this->data = array(
			'name' => $name,
			'meta_description' => $description,
			'meta_autor' => $autor,
			'redirect' => $redirect
			);
		$this->db->update($this->table_site, $this->data);
		return true;
	}

	public function f_Projects($id)
    {
		$this->id = $id;
		$this->db->where('autor', $this->id);
		$this->query = $this->db->get($this->table_projects);
		return  $this->query->num_rows(); 	
	}

	public function total_Pagination_Projects($id, $per_page, $segment)
    {
		$this->id = $id;
		$this->db->select('id, name, slug');
		$this->db->where('autor', $this->id);
		$this->query = $this->db->get($this->table_projects, $per_page, $segment);

        if ($this->query->num_rows() > 0) {
            foreach ($this->query->result() as $this->row) {
		    	$this->data[] = $this->row;
			}
            return $this->data;
        }

	}

	protected $name, $slug, $content, $id_project, $form, $id_section;
	public function new_Project($id, $name, $slug, $content)
    {
		$this->id = $id;
		$this->name = $name;
		$this->slug = $slug;
		$this->content = $content;

		$this->slug = strtolower($this->slug);
		$this->slug = url_title($this->slug, 'dash', true);

		$this->query = $this->db->where('slug', $this->slug);
		$this->query = $this->db->get($this->table_projects);

		if ($this->query->num_rows() == 0) { 
			$this->data = array(
				'autor' 	=> $this->id,
				'name' 		=> $this->name,
				'slug' 		=> $this->slug,
				'content' 	=> $this->content,
				'visibility'=> 1,
				'form' 		=> 1
				);

			$this->db->insert($this->table_projects, $this->data);
			return true;
		} else {
			return false;
		}

	}

	public function get_Project($id, $id_user)
    {
		$this->id = $id;
		$this->id_user = $id_user;
		$this->db->where('id', $this->id);
		$this->db->where('autor', $this->id_user);
		$this->query = $this->db->get($this->table_projects);

		foreach ($this->query->result() as $this->data) {
			$this->id_autor = $this->data->autor;
		}

		if ($this->id_autor == $this->id_user) {
			return $this->query->result();
		} else {
			return $this->query = null;
		}
		
	}

	public function edit_Project($id, $name, $content, $visibility, $form)
    {
		$this->id = $id;
		$this->name = $name;
		$this->content = $content;
		$this->visibility = $visibility;
		$this->form = $form;

		$this->data = array(
			'name' => $this->name,
			'content' => $this->content,
			'visibility' => $this->visibility,
			'form' => $this->form
			);

		$this->db->where('id', $this->id);
		$this->db->update($this->table_projects, $this->data);
	}

	public function delete_Project($id_user, $id)
    {
		$this->id_autor = $id_user;
		$this->id = $id;

		$this->db->where('id', $this->id);
		$this->db->where('autor', $this->id_autor);
		$this->db->delete($this->table_projects);

		$this->db->where('id_project', $this->id);
		$this->db->delete($this->table_sections);
		return true;
	}

	/* ----------- End Projects ----------- */



	/* ----------- Sections ----------- */

	public function new_Section($id_autor, $id_project, $name, $slug)
    {
		$this->id_autor = $id_autor;
		$this->id_project = $id_project;
		$this->name = $name;
		$this->slug = $slug;

		$this->slug = strtolower($this->slug);
		$this->slug = url_title($this->slug, 'dash', true);

		$this->data = array(
			'autor' 		=> $this->id_autor,
			'id_project' 	=> $this->id_project,
			'name' 			=> $this->name,
			'slug' 			=> $this->slug,
			'visibility'	=> 1
			);

		$this->db->insert($this->table_sections, $this->data);
		return true;
	}

	public function get_Sections($id_user, $id_project)
    {
		$this->id_autor = $id_user;
		$this->id_project = $id_project;

		$this->db->where('id_project', $this->id_project);
		$this->db->where('autor', $this->id_autor);
		$this->query = $this->db->get($this->table_sections);

		return $this->query->result();
	}

	public function get_Num_Sections($id_project)
    {
		$this->id_project = $id_project;

		$this->db->where('id_project', $this->id_project);
		$this->query = $this->db->get($this->table_sections);

		return $this->query->num_rows();
	}

	public function get_Section($id_user, $id_section)
    {
		$this->id_user = $id_user;
		$this->id_section = $id_section;

		$this->db->where('id', $this->id_section);
		$this->db->where('autor', $this->id_user);
		$this->query = $this->db->get($this->table_sections);
		return $this->query->result();
	}

	public function update_Section($id, $id_autor, $name, $content, $visibility)
    {
		$this->id = $id;
		$this->id_autor = $id_autor;
		$this->name = $name;
		$this->content = $content;
		$this->visibility = $visibility;

		$this->data = array(
			'name' 			=> $this->name,
			'content' 		=> $this->content,
			'visibility' 	=> $this->visibility
			);

		$this->db->where('id', $this->id);
		$this->db->where('autor', $this->id_autor);
		$this->db->update($this->table_sections, $this->data);
	}

	public function delete_Section($id, $id_autor)
    {
		$this->id = $id;
		$this->id_autor = $id_autor;

		$this->db->where('id', $this->id);
		$this->db->where('autor', $this->id_autor);
		$this->db->delete($this->table_sections);
		return true;
	}

	/* ----------- End Sections ----------- */



	/* ----------- Messages ----------- */
	public function f_Messages($id)
    {
		$this->id = $id;
		$this->db->where('autor', $this->id);
		$this->query = $this->db->get($this->table_messages);
		return  $this->query->num_rows(); 	
	}

	public function total_Pagination_Messages($id, $per_page, $segment)
    {
		$this->id = $id;
		$this->db->where('autor', $this->id);
		$this->query = $this->db->get($this->table_messages, $per_page, $segment);

        if ($this->query->num_rows() > 0) {
            foreach ($this->query->result() as $this->row) {
		    	$this->data[] = $this->row;
			}
            return $this->data;
        }

	}

	public function get_Message($id_autor, $id)
    {
		$this->id_autor = $id_autor;
		$this->id = $id;

		$this->db->where('id', $this->id);
		$this->db->where('autor', $this->id_autor);
		$this->query = $this->db->get($this->table_messages);

		if (!empty($this->query)) {
			return $this->query->result();
		} else {
			return null;
		}

	}

	public function delete_Message($id_autor, $id)
    {
		$this->id_autor = $id_autor;
		$this->id = $id;

		$this->db->where('id', $this->id);
		$this->db->where('autor', $this->id_autor);
		$this->db->delete($this->table_messages);
		return true;			
	}

	public function delete_All($id)
    {
		$this->id = $id;
		$this->db->where('autor', $this->id);
		$this->db->delete($this->table_messages);
		return true;
	}

	public function get_Project_Name($id)
    {
		$this->id = $id;
		$this->db->select('name');
		$this->db->where('id', $this->id);
		$this->query = $this->db->get($this->table_projects);

		foreach ($this->query->result() as $this->data) {
			$this->name = $this->data->name;
		}

		echo $this->name;
	}

	/* ----------- End Messages ----------- */



	/* ----------- Profile ----------- */
	protected $lastname, $old_password, $new_password;
	public function get_Profile($id)
    {
		$this->id_user = $id;

		$this->db->select('email, name, lastname');
		$this->db->where('id', $this->id_user);
		$this->query = $this->db->get($this->table_users);

		return $this->query->result();
	}

	public function update_Profile($id, $name, $lastname)
    {
		$this->id_user = $id;
		$this->name = $name;
		$this->lastname = $lastname;

		$this->data = array(
			'name' => $name,
			'lastname' => $lastname
			);

		$this->db->where('id', $this->id_user);
		$this->db->update($this->table_users, $this->data);
		return true;
	}

	public function update_Password($id, $old_password, $new_password)
    {
		$this->id_user = $id;
		$this->old_password = sha1($old_password);
		$this->new_password = sha1($new_password);

		$this->db->select('password');
		$this->db->where('id', $this->id_user);
		$this->query = $this->db->get($this->table_users);

		foreach ($this->query->result() as $pass) {

			if ($pass->password == $this->old_password) {
				$this->data = array(
        			'password' => $this->new_password
				);

				$this->db->where('id', $this->id_user);
				$this->db->update($this->table_users, $this->data);
				return true;
			} else {
				return false;
			}

		}

	}

}