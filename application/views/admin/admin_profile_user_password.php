<!DOCTYPE html>
<html>
<head>
<title>Easy Documentor > My Account > Password</title>
<?= link_tag('assets/css/bootstrap.css'); ?>
<?= link_tag('assets/css/app.css'); ?>
<?= get_font_family(); ?>
</head>
<body>
<?= menu(3); ?>

<div class="container">

<h2><b class="glyphicon glyphicon-user"> </b>  My Account > Password </h2>

<div class="col-md-3">
<div class="list-group">
<a href="<?= base_url('administrator/my_account/'); ?>" class="list-group-item">My Account</a>
<a class="list-group-item active">Password</a>
</div>
</div>

<div class="col-md-9">
<div class="panel panel-default">
<div class="panel-body">

<?php if (isset($message_alert)): ?>
	<div class="alert alert-danger"><strong>Oops! </strong><?= $message_alert; ?></div>
<?php endif ?>
	
<?php if (validation_errors()): ?>
	<div class="alert alert-danger"><?= validation_errors(); ?></div>
<?php endif ?>

<?= form_open(); ?>
	<?= form_label('Old password: '); ?>
	<?= form_input(admin_profile_old_password()); ?>
	<br>
	<?= form_label('New password: '); ?>
	<?= form_input(admin_profile_new_password()); ?>
	<br>
	<?= form_label('Repeat password: '); ?>
	<?= form_input(admin_profile_new_password_repeat()); ?>
	<br>
	<?= form_input(admin_profile__password_submit()); ?>
<?= form_close(); ?>


</div>
</div>
</div>

</div>


<?= link_tag_script('//ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js'); ?>
<?= link_tag_js('assets/js/bootstrap.js'); ?>
</body>
</html>