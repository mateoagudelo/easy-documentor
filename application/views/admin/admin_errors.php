<!DOCTYPE html>
<html>
<head>
<title>Easy Documentor > An error has occurred.</title>
<?= link_tag('assets/css/bootstrap.css'); ?>
<?= link_tag('assets/css/app.css'); ?>
<?= get_font_family(); ?>
</head>
<body>

<div class="container">
<div class="panel panel-danger" style="margin-top: 100px;">
<div class="panel-heading"><strong></strong><?= $title; ?></div>
<div class="panel-body">
<?= $problem; ?>
</div>	
</div>
</div>

</body>
</html>