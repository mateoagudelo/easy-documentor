<!DOCTYPE html>
<html>
<head>
<title>Easy Documentor > Projects > Edit</title>
<?= link_tag('assets/css/bootstrap.css'); ?>
<?= link_tag('assets/css/app.css'); ?>
<?= link_tag('assets/css/trumbowyg.min.css'); ?>
<?= get_font_family(); ?>
</head>
<body>
<?= menu(1); ?>

<?php

if (!empty($projects)) {
foreach ($projects as $project) {$id_p = $project->id; $name = $project->name; $slug = $project->slug; $content = $project->content; $visi = $project->visibility; $form = $project->form; }	
} else {
	echo 'No hay na.';
}
?>

<div class="container">

<div class="col-md-9">

<h2>Edit project</h2>

<?php if (validation_errors()): ?>
	<div class="alert alert-danger"><?= validation_errors(); ?></div>
<?php endif ?>

<?= form_open(); ?>

<?= form_input(admin_Edit_Project_Hidden(1)); ?>

<?= form_input(admin_Edit_Project_Name($name)); ?>
<br>
		
<?= form_label('Slug: '); ?>
<?= form_input(admin_Edit_Project_Slug($slug)); ?>

<?= form_textarea(admin_Edit_Project_Content($content)); ?>
<br>

<div class="panel panel-default">
<div class="panel-heading"><strong>Sections</strong> <div class="pull-right"><button type="button" class="btn btn-success btn-xs" data-toggle="modal" data-target="#myModal"><b class="glyphicon glyphicon-plus"></b> Add new</button></div></div>
<div class="panel-body">


<?php if (!empty($sections)): ?>
<table class="table table-condensed">
<thead>
<th>Name</th>
<th>Slug</th>
<th>Options</th>
</thead>

<?php foreach ($sections as $section): ?>
<tr>
<td><?= $section->name; ?></td>
<td><?= $section->slug; ?></td>
<td><a href="<?= base_url('administrator/sections/edit/'.$section->id.'/'); ?>" class="btn btn-info btn-xs"><b class="glyphicon glyphicon-pencil"></b></a> <a href="<?= base_url('administrator/sections/delete/'.$section->id.'/'); ?>" class="btn btn-danger btn-xs"><b class="glyphicon glyphicon-remove"></b></a></td>
</tr>
<?php endforeach ?>

</table>
<?php else: ?>
<div class="alert alert-danger"><strong>Oops! </strong>No found records.</div>
<?php endif ?>


</div>
</div>


</div>

<div class="col-md-3">

<div class="panel panel-warning">
<div class="panel-heading"><strong>Options</strong></div>
<div class="panel-body">

<?= form_label('Contact form: ') ?>
<?= admin_Edit_Project_Form($form); ?>
<br>

<?= form_label('Visibility: ') ?>
<?= admin_Edit_Project_Visibility($visi); ?>
<br>

<?= form_input(admin_Edit_Project_Submit()); ?>

<?= form_close(); ?>
</div>
</div>

<a href="<?= base_url('administrator/projects/delete/'.$id_p.'/');  ?>"><b class="glyphicon glyphicon-remove"></b> Delete</a>

</div>

<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
<div class="modal-dialog">
<div class="modal-content">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal">&times;</button>
<h4 class="modal-title">Add new section</h4>
</div>
<div class="modal-body">
<?= form_open(); ?>
<?php if (validation_errors()): ?>
  <div class="alert alert-danger"><?= validation_errors(); ?></div>
<?php endif ?>


<?= form_input(admin_Edit_Project_Hidden(2)); ?>
<?= form_label('Name: '); ?>
<?= form_input(admin_New_Section_Name()); ?>
<br>
<?= form_label('Slug: '); ?>
<?= form_input(admin_New_Section_Slug()); ?>
</div>
<div class="modal-footer">
<?= form_input(admin_New_Section_Submit()); ?>
<?= form_close(); ?>
</div>
</div>
</div>
</div>
<!-- End Modal -->

</div>

<?= link_tag_script('//ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js'); ?>
<?= link_tag_script(base_url('assets/js/trumbowyg.min.js')); ?>
<?= link_tag_js('assets/js/bootstrap.js'); ?>

<!-- Open -->
<script type="text/javascript">
var base_url = '<?php echo base_url(); ?>';
$.trumbowyg.svgPath = base_url+'assets/js/icons.svg';
$('#content').trumbowyg();  
</script>
<!-- Close -->

</body>
</html>