<!DOCTYPE html>
<html>
<head>
<title>Easy Documentor > Login</title>
<?= link_tag('assets/css/bootstrap.css'); ?>
<?= link_tag('assets/css/app.css'); ?>
<?= get_font_family(); ?>
</head>
<body>

<div class="container">
<div class="row">
<div class="col-md-8 col-md-offset-2">
<div class="panel panel-default" style="margin-top: 100px;">
<div class="panel-heading">Login</div>
<div class="panel-body">
<?php if (validation_errors()): ?>
<div class="alert alert-danger"><?= validation_errors(); ?></div>
<?php endif ?>
<?= form_open(null, array('class' => 'form-horizontal')); ?>
<div class="form-group">
<label for="email" class="col-md-4 control-label">E-Mail Address</label>
<div class="col-md-6"><?= form_input(admin_login_set_email()); ?></div>
</div>

<div class="form-group">
<label for="password" class="col-md-4 control-label">Password</label>
<div class="col-md-6"><?= form_input(admin_login_set_password()); ?></div>
</div>

<div class="form-group">
<div class="col-md-8 col-md-offset-4"><?= form_input(admin_login_set_input()); ?></div>
</div>
<?= form_close(); ?>

</div>
</div>
</div>
</div>

</body>
</html>