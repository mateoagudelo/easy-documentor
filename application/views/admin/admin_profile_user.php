<!DOCTYPE html>
<html>
<head>
<title>Easy Documentor > My Account</title>
<?= link_tag('assets/css/bootstrap.css'); ?>
<?= link_tag('assets/css/app.css'); ?>
<?= get_font_family(); ?>
</head>
<body>
<?= menu(3); ?>

<?php foreach ($users as $user) {
	$name = $user->name; $last = $user->lastname; $email = $user->email;
}  ?>

<div class="container">

<h2><b class="glyphicon glyphicon-user"> </b>  My Account </h2>

<div class="col-md-3">
<div class="list-group">
  <a class="list-group-item active">My Account</a>
  <a href="<?= base_url('administrator/my_account/password/'); ?>" class="list-group-item">Password</a>
</div>
</div>

<div class="col-md-9">
<div class="panel panel-default">
	<div class="panel-body">

		<?php if (validation_errors()): ?>
			<div class="alert alert-danger"><?= validation_errors(); ?></div>
		<?php endif ?>

		<?= form_open(); ?>
			<?= form_label('Name: ') ?>
			<?= form_input(admin_profile_name($name)); ?>
			<br>

			<?= form_label('Lastname: ') ?>
			<?= form_input(admin_profile_lastname($last)); ?>
			<br>

			<?= form_label('E-mail: ') ?>
			<?= form_input(admin_profile_email($email)); ?>
			<br>

			<?= form_input(admin_profile_submit()); ?>
		<?= form_close(); ?>
	</div>
</div>
</div>


</div>


<?= link_tag_script('//ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js'); ?>
<?= link_tag_js('assets/js/bootstrap.js'); ?>
</body>
</html>