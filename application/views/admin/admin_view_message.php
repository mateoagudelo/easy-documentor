<!DOCTYPE html>
<html>
<head>
<title>Easy Documentor > Messages > View</title>
<?= link_tag('assets/css/bootstrap.css'); ?>
<?= link_tag('assets/css/app.css'); ?>
<?= get_font_family(); ?>
</head>
<body>
<?= menu(2); ?>

<?php 
foreach ($messages as $message) {
	$id = $message->id; $name = $message->name;	 $msj = $message->message; $email = $message->email; $project = $message->id_project; $date = $message->date;
} 
?>

<div class="container">
<div class="col-md-8">
<div class="panel panel-default">
<div class="panel-heading"><strong>From: </strong> <?= $name; ?></div>
<div class="panel-body">
<?= $msj; ?>
</div>
</div>
</div>

<div class="col-md-4">
<div class="panel panel-info">
<div class="panel-heading"><strong>Info</strong></div>
<div class="panel-body">
<strong>Email: </strong> <?= $email; ?> <br>
<strong>Project: </strong> <?php $model->Get_Project_Name($project); ?> <br>
<strong>Date: </strong> <?= $date; ?> <br>
</div>
</div>
<a href="<?= base_url('administrator/messages/delete/'.$id.'/'); ?>"><b class="glyphicon glyphicon-remove"></b> Delete</a>
</div>
</div>


<?= link_tag_script('//ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js'); ?>
<?= link_tag_js('assets/js/bootstrap.js'); ?>
</body>
</html>