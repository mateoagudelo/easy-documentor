<!DOCTYPE html>
<html>
<head>
<title>Easy Documentor > Sections > Delete</title>
<?= link_tag('assets/css/bootstrap.css'); ?>
<?= link_tag('assets/css/app.css'); ?>
<?= get_font_family(); ?>
</head>
<body>
<?= menu(1); ?>


<div class="container">

<div class="col-md-6 col-md-offset-3">
<div class="panel panel-danger">
	<div class="panel-heading"><a href="<?= base_url('administrator/sections/edit/'.$id.'/'); ?>" class="btn btn-default btn-xs"><b class="glyphicon glyphicon-arrow-left"></b> Back</a><strong> Delete</strong></div>
	<div class="panel-body">
		<center style="margin-top: -20px;"><h2>¿Are you sure?</h2></center>

		<?= form_open(); ?>
		<input type="hidden" name="id_section" value="<?= $id; ?>">
			<center><?= form_input(admin_delete_section_submit()); ?></center>
		<?= form_close(); ?>
	</div>
</div>
</div>

</div>


<?= link_tag_script('//ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js'); ?>
<?= link_tag_script(base_url('assets/js/trumbowyg.min.js')); ?>
<?= link_tag_js('assets/js/bootstrap.js'); ?>

<!-- Open -->
<script type="text/javascript">
var base_url = '<?php echo base_url(); ?>';
$.trumbowyg.svgPath = base_url+'assets/js/icons.svg';
$('#content').trumbowyg();  
</script>
<!-- Close -->

</body>
</html>