<?php foreach ($configs as $config) { $title = $config->name; $description = $config->meta_description; $autor = $config->meta_autor; $redirect = $config->redirect; } ?>
<!DOCTYPE html>
<html>
<head>
<title>Easy Documentor > Config Site</title>
<?= link_tag('assets/css/bootstrap.css'); ?>
<?= link_tag('assets/css/app.css'); ?>
<?= get_font_family(); ?>
</head>
<body>
<?= menu(5); ?>

<div class="container">
<h2><b class="glyphicon glyphicon-cog"> </b>  Site settings </h2>

<div class="panel panel-default">

<div class="panel-body">

<?php if (validation_errors()): ?>
<div class="alert alert-danger"><strong>Oops!</strong> <?= validation_errors(); ?></div>
<?php endif ?>

<?= form_open(); ?>

<?= form_label('Title: '); ?>
<?= form_input(admin_config_title($title)); ?>
<br>

<?= form_label('Meta description: '); ?>
<?= form_textarea(admin_config_meta_description($description)); ?>
<br>

<?= form_label('Meta autor: '); ?>
<?= form_input(admin_config_meta_autor($autor)); ?>
<br>

<?= form_label('Redirect (home page): '); ?>
<?= form_input(admin_config_redirect($redirect)); ?>
<br>

<?= form_input(admin_config_submit()); ?>
<?= form_close(); ?>

</div>
</div>
</div>

<?= link_tag_script('//ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js'); ?>
<?= link_tag_js('assets/js/bootstrap.js'); ?>
</body>
</html>