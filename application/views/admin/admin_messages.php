<!DOCTYPE html>
<html>
<head>
<title>Easy Documentor > Messages</title>
<!DOCTYPE html>
<html>
<head>
<title></title>
<?= link_tag('assets/css/bootstrap.css'); ?>
<?= link_tag('assets/css/app.css'); ?>
<?= get_font_family(); ?>
</head>
<body>
<?= menu(2) ?>

<div class="container">
<h2>
<b class="glyphicon glyphicon-envelope"> </b>  Messages 

<?php if (!empty($messages)): ?>
<a href="<?= base_url('administrator/messages/delete-all/'); ?>" class="btn btn-danger btn-xs"><b class="glyphicon glyphicon-remove"></b> Delete all</a>
<?php endif ?>

</h2>

<div class="panel panel-default">
<div class="panel-body">

<?php if (!empty($messages)): ?>
<table class="table table-hover">
<thead>
<th>Project</th>
<th>From</th>
<th>Email</th>
<th>Options</th>
</thead>

<?php foreach ($messages as $message): ?>
<tr>
<td><?php $model->Get_Project_Name($message->id_project); ?></td>
<td><?= $message->name; ?></td>
<td><?= $message->email; ?></td>
<td><a href="<?= base_url('administrator/messages/view/'.$message->id.'/'); ?>" class="btn btn-info btn-xs"><b class="glyphicon glyphicon-eye-open"></b></a> <a href="<?= base_url('administrator/messages/delete/'.$message->id.'/'); ?>" class="btn btn-danger btn-xs"><b class="glyphicon glyphicon-remove"></b></a></td>
</tr>
<?php endforeach ?>

</table>
</div>
</div>

<?= $this->pagination->create_links() ?>

</div>
</div>

<?php else: ?>
<div class="alert alert-danger"><strong>Oops! </strong>No found records.</div>
<?php endif ?>

<?= link_tag_script('//ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js'); ?>
<?= link_tag_js('assets/js/bootstrap.js'); ?>

</body>
</html>