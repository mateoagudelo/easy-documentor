<!DOCTYPE html>
<html>
<head>
<title>Easy Documentor > Projects</title>
<?= link_tag('assets/css/bootstrap.css'); ?>
<?= link_tag('assets/css/app.css'); ?>
<?= get_font_family(); ?>
</head>
<body>
<?= menu(1) ?>

<div class="container">

<h2><b class="glyphicon glyphicon-folder-open"> </b>  Projects <a href="<?= base_url('administrator/projects/new/'); ?>" class="btn btn-success btn-xs"><b class="glyphicon glyphicon-plus"></b> Add new</a></h2>
<?php if (!empty($projects)): ?>
<?php foreach ($projects as $project): ?>
<div class="col-md-6">
<div class="panel panel-default">
<div class="panel-body">	
<a href="<?= base_url('administrator/projects/edit/'.$project->id.'/'); ?>"><b class="glyphicon glyphicon-folder-open"></b> <?= $project->name; ?> <h5><?= $model->Get_Num_Sections($project->id); ?> Sections</h5></a>
<div class="pull-right">
<div class="dropdown">
<button class="btn btn-default dropdown-toggle btn-xs" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">Options
<span class="caret"></span>
</button>
<ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
<li><a target="_blank" href="<?= base_url('project/'.$project->slug.'/'); ?>"><b class="glyphicon glyphicon-eye-open"></b> View</a></li>
<li><a href="<?= base_url('administrator/projects/edit/'.$project->id.'/'); ?>"><b class="glyphicon glyphicon-pencil"></b> Edit</a></li>
<li role="separator" class="divider"></li>
<li><a href="<?= base_url('administrator/projects/delete/'.$project->id.'/'); ?>"><b class="glyphicon glyphicon-remove"></b> Delete</a></li>
</ul>
</div>
</div>
</div>
</div>
</div>
<?php endforeach ?>
<?php else: ?>
<div class="alert alert-danger"><strong>Oops! </strong>Not found records.</div>
<?php endif ?>
<div class="col-md-12"><?= $this->pagination->create_links() ?></div>

</div>

<?= link_tag_script('https://code.jquery.com/jquery-1.10.2.min.js'); ?>
<?= link_tag_js('assets/js/bootstrap.js'); ?>
</body>
</html>