<!DOCTYPE html>
<html>
<head>
<title>Easy Documentor > Sections > Edit</title>
<?= link_tag('assets/css/bootstrap.css'); ?>
<?= link_tag('assets/css/app.css'); ?>
<?= link_tag('assets/css/trumbowyg.min.css'); ?>
<?= get_font_family(); ?>
</head>
<body>
<?= menu(1); ?>

<?php foreach ($sections as $section): ?>
<div class="container">
	<div class="col-md-9">
	<div class="panel panel-default">
	<div class="panel-heading"><a href="<?= base_url('administrator/projects/edit/'.$section->id_project.'/'); ?>" class="btn btn-danger btn-xs"><b class="glyphicon glyphicon-arrow-left"></b> back</a><strong> Edit Section</strong></div>
	<div class="panel-body">
	<?php if (validation_errors()): ?>
	<div class="alert alert-danger"><?= validation_errors(); ?></div>
	<?php endif ?>
		<?= form_open(); ?>

		<?= form_label('Name: '); ?>
		<?= form_input(admin_edit_section_name($section->name)); ?>
		<br>

		<?= form_label('Slug: '); ?>
		<?= form_input(admin_edit_section_slug($section->slug)); ?>
		<br>

		<hr>

		<?= form_label('Content: '); ?>
		<?= form_textarea(admin_edit_section_content($section->content)); ?>
		<br>

	</div>
	</div>
	</div>

	<div class="col-md-3">
		<div class="panel panel-warning">
			<div class="panel-heading"><strong>Options</strong></div>
			<div class="panel-body">
				<?= form_label('Visibility: '); ?>
				<?= admin_edit_section_visibility($section->visibility); ?>
				<br>
				<?= form_input(admin_edit_section_submit()); ?>
				<?= form_close(); ?>
			</div>
		</div>
		<a href="<?= base_url('administrator/sections/delete/'.$section->id.'/');  ?>"><b class="glyphicon glyphicon-remove"></b> Delete</a>
	</div>
</div>
<?php endforeach ?>



<?= link_tag_script('//ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js'); ?>
<?= link_tag_script(base_url('assets/js/trumbowyg.min.js')); ?>
<?= link_tag_js('assets/js/bootstrap.js'); ?>

<!-- Open -->
<script type="text/javascript">
var base_url = '<?php echo base_url(); ?>';
$.trumbowyg.svgPath = base_url+'assets/js/icons.svg';
$('#content_section').trumbowyg();  
</script>
<!-- Close -->

</body>
</html>