<!DOCTYPE html>
<html lang="en">
<head>
<title>Easy Documentor > Projects > New</title>
<?= link_tag('assets/css/bootstrap.css'); ?>
<?= link_tag('assets/css/app.css'); ?>
<?= link_tag('assets/css/trumbowyg.min.css'); ?>
<?= get_font_family(); ?>

</head>
<body>
<?= menu(1) ?>

<div class="container">
	
<div class="col-md-9">
<?php if (validation_errors()): ?>
	<div class="alert alert-danger"><?= validation_errors(); ?></div>
<?php endif ?>

<?php if (isset($msj)): ?>
	<div class="alert alert-<?= $color; ?>"><?= $msj; ?></div>
<?php endif ?>

<?= form_open(); ?>
<h2>New project</h2>
<?= form_input(admin_New_Project_Name()); ?>
<br>

<?= form_label('Slug: '); ?>
<?= form_input(admin_New_Project_Slug()); ?>


<?= form_textarea(admin_New_Project_Content()); ?>

<br>

</div>

<div class="col-md-3">
<div class="panel panel-warning">
<div class="panel-heading"><strong>Options</strong></div>

<div class="panel-body">
<label>Visibility: </label>
<strong>Public</strong>
</div>

<div class="panel-footer" style="padding: 25px;">
<div class="pull-right" style="margin-top: -15px; margin-right: -10px;"><?= form_input(admin_New_Project_Submit()); ?></div>
<?= form_close(); ?>
</div>
</div>
</div>

</div>

<?= link_tag_script('//ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js'); ?>
<?= link_tag_script(base_url('assets/js/trumbowyg.min.js')); ?>
<?= link_tag_js('assets/js/bootstrap.js'); ?>

<!-- Open -->
<script type="text/javascript">
var base_url = '<?php echo base_url(); ?>';
$.trumbowyg.svgPath = base_url+'assets/js/icons.svg';
$('#content').trumbowyg();	
</script>
<!-- Close -->

</body>
</html>