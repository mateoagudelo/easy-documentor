<!DOCTYPE html>
<html>
<head>
<title>Easy Documentor > Media</title>
<?= link_tag('assets/css/bootstrap.css'); ?>
<?= link_tag('assets/css/app.css'); ?>
<?= get_font_family(); ?>
</head>
<body>
<?= menu(4); ?>

<div class="container">
	
<div class="col-md-12">
<?=@$error?>
<?php if (validation_errors()): ?>
<div class="alert alert-danger"><?= validation_errors(); ?></div>
<?php endif ?>
<button id="open_form" onclick="show_upload_form()" class="btn btn-success btn-xs"><b class="glyphicon glyphicon-plus"></b> Add file</button>
<div id="form_upload_image">
<div class="panel panel-default">
<div class="panel-body">
<?=form_open_multipart(base_url('media/do_upload'))?>
<label>Title: </label>
<input type="text" class="form-control" name="title">
<br>
<label>Image: </label>
<input type="file" name="userfile" />
<br>
<input type="submit" class="btn btn-success btn-xs" value="Upload" /> <div id="close_form" onclick="close_upload_form()" class="btn btn-danger btn-xs"><b class="glyphicon glyphicon-remove"></b> Cancel</div>
<?=form_close()?>
<br>
</div>
</div>
</div>
</div>

<div class="col-md-12"><h2><b class="glyphicon glyphicon-picture"> </b> Media</h2></div>
<?php if (!empty($images)): ?>
<?php foreach ($images as $image): ?>
<div class="col-md-3">
<div class="panel panel-default">
<div class="panel-body">
<center><a href="<?= base_url('administrator/media/view/'.$image->id.'/'); ?>"><?= $image->title; ?></a></center>
<center><img width="150" height="90" src="<?= base_url('uploads/'.$image->uri); ?>" ></center>
<br>
<input class="form-control input-sm" type="text" readonly="" value="<?= base_url('uploads/'.$image->uri); ?>">
</div>
</div>
</div>
<?php endforeach ?>
<?php else: ?>
<div class="col-md-12">
<div class="alert alert-danger"><strong>Oops! </strong>No found records.</div>
</div>
<?php endif ?>

<div class="col-md-12"><?= $this->pagination->create_links() ?></div>

</div>

<?= link_tag_script('//ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js'); ?>
<?= link_tag_js('assets/js/app.js'); ?>
<?= link_tag_js('assets/js/bootstrap.js'); ?>
</body>
</html>