<!DOCTYPE html>
<html>
<head>
<title>Easy Documentor > Media > View Image</title>
<?= link_tag('assets/css/bootstrap.css'); ?>
<?= link_tag('assets/css/app.css'); ?>
<?= get_font_family(); ?>
</head>
<body>
<?= menu(4); ?>

<div class="container">
<?php foreach ($images as $image): ?>
<div class="col-md-9">
<div class="panel panel-default">
<div class="panel-heading"><strong><a href="<?= base_url('administrator/media/'); ?>" class="btn btn-danger btn-xs"><b class="glyphicon glyphicon-arrow-left"></b> back</a> View -> <?= $image->title; ?></strong></div>
<div class="panel-body">
<center><img class="img-responsive" src="<?= base_url('uploads/'.$image->uri); ?>"></center>
</div>
</div>
</div>

<div class="col-md-3">
<div class="panel panel-info">
<div class="panel-heading"><strong>Info</strong></div>
<div class="panel-body">
			
<strong>Name: </strong> <?= $image->title;  ?>
<br>
<strong>Image original: </strong> <p><?= $image->uri; ?></p>

</div>
</div>
<a href="<?= base_url('administrator/media/delete/'.$image->id); ?>">Delete</a>
</div>
<?php endforeach ?>
</div>

<?= link_tag_script('//ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js'); ?>
<?= link_tag_js('assets/js/bootstrap.js'); ?>
</body>
</html>