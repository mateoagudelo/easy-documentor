<?php foreach ($content as $value) { $id = $value->id; $name = $value->name; $content = $value->content; $slug = $value->slug; } ?>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, shrink-to-fit=no, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<title>Project > <?= $name; ?></title>
<?= link_tag('assets/css/bootstrap.css'); ?>
<?= link_tag('assets/css/simple-sidebar.css'); ?>
<?= link_tag('assets/css/public.css'); ?>
<?= get_public_font_family(); ?>
<?= get_public_font_family_2(); ?>
<?= get_public_font_family_3(); ?>
</head>
<body>


<div id="wrapper" class="toggled">

<!-- Sidebar -->
<div id="sidebar-wrapper">
<ul class="sidebar-nav">
<li class="sidebar-brand">
<a id="title_documentation" href="<?= base_url('project/'.$slug.'/'); ?>">Documentation</a>
</li>
<?php foreach ($sections as $section): ?>
<li><a href="<?= base_url('project/'.$slug.'/'.$section->slug.'/'); ?>"><?= $section->name; ?></a></li>
<?php endforeach ?>
<li><a>Contact</a></li>
</ul>
</div>
<!-- /#sidebar-wrapper -->

<!-- Page Content -->
<div id="page-content-wrapper">
<div class="container-fluid">
<div class="row">
<div class="col-lg-12">

<ul class="breadcrumb">
  <li class="active"><a href="#">Docs</a></li>
  <li class="active"><a href="<?= base_url('project/'.$slug.'/'); ?>" ><?= $name; ?></a></li>
  <li class="active">Contact</li>
</ul>

<hr>

<h1>Contact</h1>

<div class="content"><p>

<?php if (validation_errors()): ?>
	<div class="alert alert-danger"><strong>Oops! </strong><?= validation_errors(); ?></div>
<?php endif ?>

<?= form_open(); ?>

<input type="hidden" name="id_project" value="<?= $id; ?>" name="">
<?= form_label('Name: ', 'name') ?>
<?= form_input(array('type' => 'text', 'name' => 'name', 'id' => 'name', 'class' => 'form-control', 'required' => 'on')); ?>
<br>

<?= form_label('Email: ', 'email') ?>
<?= form_input(array('type' => 'email', 'name' => 'email', 'id' => 'email', 'class' => 'form-control', 'required' => 'on')); ?>
<br>

<?= form_label('Message: ', 'message') ?>
<?= form_textarea(array('rows' => '4', 'name' => 'message', 'id' => 'message', 'class' => 'form-control', 'required' => 'on')); ?>
<br>

<?= form_input(array('type' => 'submit', 'value' => 'Send', 'class' => 'btn btn-success')); ?>

<?= form_close(); ?>
</p></div>

<a href="#menu-toggle" class="btn btn-default" id="menu-toggle">Toggle Menu</a>
</div>
</div>
</div>
</div>
<!-- /#page-content-wrapper -->


</div>
<!-- /#wrapper -->


<?= link_tag_script('https://code.jquery.com/jquery-1.10.2.min.js'); ?>
<?= link_tag_js('assets/js/bootstrap.js'); ?>
<!-- Menu Toggle Script -->
<script>
$("#menu-toggle").click(function(e) {
    e.preventDefault();
    $("#wrapper").toggleClass("toggled");
});
</script>

</body>
</html>