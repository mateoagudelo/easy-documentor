<?php foreach ($content as $value) { $name = $value->name; $content = $value->content; $slug = $value->slug; $form = $value->form; $vi = $value->visibility; } 

if ($vi == 0) { redirect(base_url()); }
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, shrink-to-fit=no, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<title>Project > <?= $name; ?></title>
<?= link_tag('assets/css/bootstrap.css'); ?>
<?= link_tag('assets/css/simple-sidebar.css'); ?>
<?= link_tag('assets/css/public.css'); ?>
<?= get_public_font_family(); ?>
<?= get_public_font_family_2(); ?>
<?= get_public_font_family_3(); ?>
</head>
<body>

<div id="wrapper" class="toggled">
<!-- Sidebar -->
<div id="sidebar-wrapper">
<ul class="sidebar-nav">
<li class="sidebar-brand">
<a id="title_documentation" href="<?= base_url('project/'.$slug.'/'); ?>">Documentation</a>
</li>
<?php foreach ($sections as $section): ?>
<li><a href="<?= base_url('project/'.$slug.'/'.$section->slug.'/'); ?>"><?= $section->name; ?></a></li>
<?php endforeach ?>
<?php if ($form == 1): ?>
<li><a href="<?= base_url('contact/'.$slug.'/'); ?>">Contact</a></li>
<?php endif ?>
</ul>
</div>
<!-- /#sidebar-wrapper -->

<!-- Page Content -->
<div id="page-content-wrapper">
<div class="container-fluid">
<div class="row">
<div class="col-lg-12">

<ul class="breadcrumb">
  <li class="active"><a href="#">Docs</a></li>
  <li class="active"><?= $name; ?></li>
</ul>

<hr>

<h1><?= $name; ?></h1>
<div class="content"><p><?= $content; ?></p></div>
<a href="#menu-toggle" class="btn btn-default" id="menu-toggle">Toggle Menu</a>
</div>
</div>
</div>
</div>

<!-- /#page-content-wrapper -->


</div>
<!-- /#wrapper -->

</div>


<?= link_tag_script('https://code.jquery.com/jquery-1.10.2.min.js'); ?>
<?= link_tag_js('assets/js/bootstrap.js'); ?>
<!-- Menu Toggle Script -->
<script>
$("#menu-toggle").click(function(e) {
    e.preventDefault();
    $("#wrapper").toggleClass("toggled");
});
</script>

</body>
</html>